// HW154.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

int const N = 13;

void GetNumbers(int n, bool isOdd)
{
    for (int i = !isOdd; i <= n; i+=2)
    {
        std::cout << i << '\n';
    }
}

int main()
{
    GetNumbers(N, true);
}


