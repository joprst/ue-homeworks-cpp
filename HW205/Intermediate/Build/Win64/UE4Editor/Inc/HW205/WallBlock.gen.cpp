// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HW205/WallBlock.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWallBlock() {}
// Cross Module References
	HW205_API UClass* Z_Construct_UClass_AWallBlock_NoRegister();
	HW205_API UClass* Z_Construct_UClass_AWallBlock();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_HW205();
	HW205_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	void AWallBlock::StaticRegisterNativesAWallBlock()
	{
	}
	UClass* Z_Construct_UClass_AWallBlock_NoRegister()
	{
		return AWallBlock::StaticClass();
	}
	struct Z_Construct_UClass_AWallBlock_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AWallBlock_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_HW205,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWallBlock_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "WallBlock.h" },
		{ "ModuleRelativePath", "WallBlock.h" },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_AWallBlock_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(AWallBlock, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AWallBlock_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AWallBlock>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AWallBlock_Statics::ClassParams = {
		&AWallBlock::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AWallBlock_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AWallBlock_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AWallBlock()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AWallBlock_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AWallBlock, 3508089208);
	template<> HW205_API UClass* StaticClass<AWallBlock>()
	{
		return AWallBlock::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AWallBlock(Z_Construct_UClass_AWallBlock, &AWallBlock::StaticClass, TEXT("/Script/HW205"), TEXT("AWallBlock"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWallBlock);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
