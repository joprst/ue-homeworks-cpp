// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HW205_PlayerPawn_generated_h
#error "PlayerPawn.generated.h already included, missing '#pragma once' in PlayerPawn.h"
#endif
#define HW205_PlayerPawn_generated_h

#define HW205_Source_HW205_PlayerPawn_h_16_SPARSE_DATA
#define HW205_Source_HW205_PlayerPawn_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execHorizontalInputreceiver); \
	DECLARE_FUNCTION(execVerticalInputReceiver);


#define HW205_Source_HW205_PlayerPawn_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execHorizontalInputreceiver); \
	DECLARE_FUNCTION(execVerticalInputReceiver);


#define HW205_Source_HW205_PlayerPawn_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlayerPawn(); \
	friend struct Z_Construct_UClass_APlayerPawn_Statics; \
public: \
	DECLARE_CLASS(APlayerPawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/HW205"), NO_API) \
	DECLARE_SERIALIZER(APlayerPawn)


#define HW205_Source_HW205_PlayerPawn_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAPlayerPawn(); \
	friend struct Z_Construct_UClass_APlayerPawn_Statics; \
public: \
	DECLARE_CLASS(APlayerPawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/HW205"), NO_API) \
	DECLARE_SERIALIZER(APlayerPawn)


#define HW205_Source_HW205_PlayerPawn_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlayerPawn(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlayerPawn) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerPawn); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerPawn); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerPawn(APlayerPawn&&); \
	NO_API APlayerPawn(const APlayerPawn&); \
public:


#define HW205_Source_HW205_PlayerPawn_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerPawn(APlayerPawn&&); \
	NO_API APlayerPawn(const APlayerPawn&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerPawn); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerPawn); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APlayerPawn)


#define HW205_Source_HW205_PlayerPawn_h_16_PRIVATE_PROPERTY_OFFSET
#define HW205_Source_HW205_PlayerPawn_h_13_PROLOG
#define HW205_Source_HW205_PlayerPawn_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HW205_Source_HW205_PlayerPawn_h_16_PRIVATE_PROPERTY_OFFSET \
	HW205_Source_HW205_PlayerPawn_h_16_SPARSE_DATA \
	HW205_Source_HW205_PlayerPawn_h_16_RPC_WRAPPERS \
	HW205_Source_HW205_PlayerPawn_h_16_INCLASS \
	HW205_Source_HW205_PlayerPawn_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define HW205_Source_HW205_PlayerPawn_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HW205_Source_HW205_PlayerPawn_h_16_PRIVATE_PROPERTY_OFFSET \
	HW205_Source_HW205_PlayerPawn_h_16_SPARSE_DATA \
	HW205_Source_HW205_PlayerPawn_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	HW205_Source_HW205_PlayerPawn_h_16_INCLASS_NO_PURE_DECLS \
	HW205_Source_HW205_PlayerPawn_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HW205_API UClass* StaticClass<class APlayerPawn>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID HW205_Source_HW205_PlayerPawn_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
