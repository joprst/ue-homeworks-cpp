// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ASnakeElementBase;
class AActor;
#ifdef HW205_ASnakeBase_generated_h
#error "ASnakeBase.generated.h already included, missing '#pragma once' in ASnakeBase.h"
#endif
#define HW205_ASnakeBase_generated_h

#define HW205_Source_HW205_ASnakeBase_h_31_SPARSE_DATA
#define HW205_Source_HW205_ASnakeBase_h_31_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnElementOverlap);


#define HW205_Source_HW205_ASnakeBase_h_31_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnElementOverlap);


#define HW205_Source_HW205_ASnakeBase_h_31_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeBase(); \
	friend struct Z_Construct_UClass_ASnakeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/HW205"), NO_API) \
	DECLARE_SERIALIZER(ASnakeBase)


#define HW205_Source_HW205_ASnakeBase_h_31_INCLASS \
private: \
	static void StaticRegisterNativesASnakeBase(); \
	friend struct Z_Construct_UClass_ASnakeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/HW205"), NO_API) \
	DECLARE_SERIALIZER(ASnakeBase)


#define HW205_Source_HW205_ASnakeBase_h_31_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeBase(ASnakeBase&&); \
	NO_API ASnakeBase(const ASnakeBase&); \
public:


#define HW205_Source_HW205_ASnakeBase_h_31_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeBase(ASnakeBase&&); \
	NO_API ASnakeBase(const ASnakeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASnakeBase)


#define HW205_Source_HW205_ASnakeBase_h_31_PRIVATE_PROPERTY_OFFSET
#define HW205_Source_HW205_ASnakeBase_h_28_PROLOG
#define HW205_Source_HW205_ASnakeBase_h_31_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HW205_Source_HW205_ASnakeBase_h_31_PRIVATE_PROPERTY_OFFSET \
	HW205_Source_HW205_ASnakeBase_h_31_SPARSE_DATA \
	HW205_Source_HW205_ASnakeBase_h_31_RPC_WRAPPERS \
	HW205_Source_HW205_ASnakeBase_h_31_INCLASS \
	HW205_Source_HW205_ASnakeBase_h_31_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define HW205_Source_HW205_ASnakeBase_h_31_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HW205_Source_HW205_ASnakeBase_h_31_PRIVATE_PROPERTY_OFFSET \
	HW205_Source_HW205_ASnakeBase_h_31_SPARSE_DATA \
	HW205_Source_HW205_ASnakeBase_h_31_RPC_WRAPPERS_NO_PURE_DECLS \
	HW205_Source_HW205_ASnakeBase_h_31_INCLASS_NO_PURE_DECLS \
	HW205_Source_HW205_ASnakeBase_h_31_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HW205_API UClass* StaticClass<class ASnakeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID HW205_Source_HW205_ASnakeBase_h


#define FOREACH_ENUM_SNAKESTATUS(op) \
	op(SnakeStatus::SPAWNING) \
	op(SnakeStatus::MOVING) \
	op(SnakeStatus::DIYING) 

enum class SnakeStatus;
template<> HW205_API UEnum* StaticEnum<SnakeStatus>();

#define FOREACH_ENUM_DIRECTION(op) \
	op(Direction::UP) \
	op(Direction::DOWN) \
	op(Direction::LEFT) \
	op(Direction::RIGHT) 

enum class Direction;
template<> HW205_API UEnum* StaticEnum<Direction>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
