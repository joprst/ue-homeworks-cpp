// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HW205_HW205GameModeBase_generated_h
#error "HW205GameModeBase.generated.h already included, missing '#pragma once' in HW205GameModeBase.h"
#endif
#define HW205_HW205GameModeBase_generated_h

#define HW205_Source_HW205_HW205GameModeBase_h_15_SPARSE_DATA
#define HW205_Source_HW205_HW205GameModeBase_h_15_RPC_WRAPPERS
#define HW205_Source_HW205_HW205GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define HW205_Source_HW205_HW205GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAHW205GameModeBase(); \
	friend struct Z_Construct_UClass_AHW205GameModeBase_Statics; \
public: \
	DECLARE_CLASS(AHW205GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/HW205"), NO_API) \
	DECLARE_SERIALIZER(AHW205GameModeBase)


#define HW205_Source_HW205_HW205GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAHW205GameModeBase(); \
	friend struct Z_Construct_UClass_AHW205GameModeBase_Statics; \
public: \
	DECLARE_CLASS(AHW205GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/HW205"), NO_API) \
	DECLARE_SERIALIZER(AHW205GameModeBase)


#define HW205_Source_HW205_HW205GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AHW205GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AHW205GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AHW205GameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AHW205GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AHW205GameModeBase(AHW205GameModeBase&&); \
	NO_API AHW205GameModeBase(const AHW205GameModeBase&); \
public:


#define HW205_Source_HW205_HW205GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AHW205GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AHW205GameModeBase(AHW205GameModeBase&&); \
	NO_API AHW205GameModeBase(const AHW205GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AHW205GameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AHW205GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AHW205GameModeBase)


#define HW205_Source_HW205_HW205GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define HW205_Source_HW205_HW205GameModeBase_h_12_PROLOG
#define HW205_Source_HW205_HW205GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HW205_Source_HW205_HW205GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	HW205_Source_HW205_HW205GameModeBase_h_15_SPARSE_DATA \
	HW205_Source_HW205_HW205GameModeBase_h_15_RPC_WRAPPERS \
	HW205_Source_HW205_HW205GameModeBase_h_15_INCLASS \
	HW205_Source_HW205_HW205GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define HW205_Source_HW205_HW205GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HW205_Source_HW205_HW205GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	HW205_Source_HW205_HW205GameModeBase_h_15_SPARSE_DATA \
	HW205_Source_HW205_HW205GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	HW205_Source_HW205_HW205GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	HW205_Source_HW205_HW205GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HW205_API UClass* StaticClass<class AHW205GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID HW205_Source_HW205_HW205GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
