// Fill out your copyright notice in the Description page of Project Settings.


#include "ASnakeBase.h"
#include "ASnakeElementBase.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementOffset = 50.f;
	Speed = 0.5f;
	LastDirection = Direction::LEFT;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	BuildSnakeBody(6);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SetActorTickInterval(Speed);
	switch (status)
	{
		case SnakeStatus::MOVING :
		{
			Move();
			break;
		}
		case SnakeStatus::DIYING :
		{
			DestroySnakeBody();
		}
	}
}

void ASnakeBase::BuildSnakeBody(int num)
{
	for (int i = 0; i != num; ++i)
	{
		FVector location = SnakeElements.Num() == 0 ? FVector(SnakeElements.Num() * ElementOffset, 0, 0) : SnakeElements[SnakeElements.Num() - 1]->GetActorLocation();
		switch (LastDirection)
		{
			case Direction::UP:
			{
				location.X -= ElementOffset;
				break;
			}
			case Direction::DOWN:
			{
				location.X += ElementOffset;
				break;
			}
			case Direction::LEFT:
			{
				location.Y += ElementOffset;
				break;
			}
			case Direction::RIGHT:
			{
				location.Y -= ElementOffset;
				break;
			}
		}
		FTransform transform(location);
		ASnakeElementBase* newElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, transform);
		newElement->Parent = this;
		int32 idx = SnakeElements.Add(newElement);
		if (idx == 0)
		{
			newElement->SetHeadMaterial();
		}
	}
}

void ASnakeBase::DestroySnakeBody()
{
	ASnakeElementBase* element = SnakeElements[SnakeElements.Num() - 1];
	if (IsValid(element))
	{
		SnakeElements.RemoveAt(SnakeElements.Num() - 1);
		element->Destroy();
	}
	if (SnakeElements.Num() == 0)
	{
		this->Destroy();
	}
}


void ASnakeBase::SnakeMustDie()
{
	status = SnakeStatus::DIYING;
}


void ASnakeBase::Move()
{
	FVector moveVector(EForceInit::ForceInitToZero);
	float delta = ElementOffset;

	switch (LastDirection)
	{
		case Direction::UP :
		{
			moveVector.X += delta;
			break;
		}
		case Direction::DOWN:
		{
			moveVector.X -= delta;
			break;
		}
		case Direction::LEFT:
		{
			moveVector.Y -= delta;
			break;
		}
		case Direction::RIGHT:
		{
			moveVector.Y += delta;
			break;
		}
	}
	SnakeElements[0]->ToggleCollision();
	for (int i = SnakeElements.Num() - 1; i != 0; i--)
	{
		ASnakeElementBase* currentElement = SnakeElements[i];
		ASnakeElementBase* prevElement = SnakeElements[i - 1];
		FVector prevLocation = prevElement->GetActorLocation();
		currentElement->SetActorLocation(prevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(moveVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::OnElementOverlap(ASnakeElementBase* element, AActor* actor)
{
	if (IsValid(element))
	{
		int32 idx = -1;
		SnakeElements.Find(element, idx);
		bool isHead = (idx == 0);
		IInteractable* interactbleObject = Cast<IInteractable>(actor);
		if (interactbleObject)
		{
			interactbleObject->Interact(this, isHead);
		}
	}
}

