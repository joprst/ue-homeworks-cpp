// Fill out your copyright notice in the Description page of Project Settings.


#include "WallBlock.h"
#include "ASnakeBase.h"

// Sets default values
AWallBlock::AWallBlock()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AWallBlock::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWallBlock::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWallBlock::Interact(AActor* interactor, bool isHead)
{
	if (isHead)
	{
		ASnakeBase* snake = Cast<ASnakeBase>(interactor);
		if (IsValid(snake))
		{
			snake->SnakeMustDie();
		}
	}
}
