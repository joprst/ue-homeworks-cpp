// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawn.h"
#include "ASnakeBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"


// Sets default values
APlayerPawn::APlayerPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;

}

// Called when the game starts or when spawned
void APlayerPawn::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();
}

void APlayerPawn::VerticalInputReceiver(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastDirection != Direction::DOWN)
		{
			SnakeActor->LastDirection = Direction::UP;
		}
		else if (value < 0 && SnakeActor->LastDirection != Direction::UP)
		{
			SnakeActor->LastDirection = Direction::DOWN;
		}
	}
}

void APlayerPawn::HorizontalInputreceiver(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastDirection != Direction::RIGHT)
		{
			SnakeActor->LastDirection = Direction::LEFT;
		}
		else if (value < 0 && SnakeActor->LastDirection != Direction::LEFT)
		{
			SnakeActor->LastDirection = Direction::RIGHT;
		}
	}
}

// Called every frame
void APlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawn::VerticalInputReceiver);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawn::HorizontalInputreceiver);
}

void APlayerPawn::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}

