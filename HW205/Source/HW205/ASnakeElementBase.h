// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "ASnakeElementBase.generated.h"

class UStaticMeshComponent;
class ASnakeBase;

UCLASS()
class HW205_API ASnakeElementBase : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeElementBase();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* StaticMesh;

	UPROPERTY()
		ASnakeBase* Parent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintNativeEvent)
		void SetHeadMaterial();
	void SetHeadMaterial_Implementation();
	virtual void Interact(AActor* interctor, bool isHead) override;

	UFUNCTION()
		void OverlapReceiver(UPrimitiveComponent* overlapedComponent, AActor* actor, UPrimitiveComponent* otherComponent, int32 bodyIndex, bool isFromSweep, const FHitResult &sweepResult);

	UFUNCTION()
		void ToggleCollision();
};
