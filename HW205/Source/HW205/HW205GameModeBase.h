// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HW205GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HW205_API AHW205GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
