// Fill out your copyright notice in the Description page of Project Settings.
#include "ASnakeElementBase.h"

#include "ASnakeBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
ASnakeElementBase::ASnakeElementBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MESHComponent"));
	StaticMesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	StaticMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
}

// Called when the game starts or when spawned
void ASnakeElementBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASnakeElementBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakeElementBase::SetHeadMaterial_Implementation()
{
	StaticMesh->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::OverlapReceiver);
}

void ASnakeElementBase::Interact(AActor* interctor, bool isHead)
{
	ASnakeBase* snake = Cast<ASnakeBase>(interctor);
	if (IsValid(snake))
	{
		snake->Destroy();
	}
}

void ASnakeElementBase::OverlapReceiver(UPrimitiveComponent* overlapedComponent, AActor* actor, UPrimitiveComponent* otherComponent, int32 bodyIndex, bool isFromSweep, const FHitResult &sweepResult)
{
	if (IsValid(Parent))
	{
		Parent->OnElementOverlap(this, actor);
	}
}

void ASnakeElementBase::ToggleCollision()
{
	if (StaticMesh->GetCollisionEnabled() == ECollisionEnabled::NoCollision)
	{
		StaticMesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
	else	
	{
		StaticMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}
