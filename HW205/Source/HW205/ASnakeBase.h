// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ASnakeBase.generated.h"

class ASnakeElementBase;

UENUM()
enum class Direction {
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UENUM()
enum class SnakeStatus {
	SPAWNING,
	MOVING,
	DIYING
};



UCLASS()
class HW205_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY()
		TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY(EditDefaultsOnly)
		float ElementOffset;

	UPROPERTY()
		float Speed;

	UPROPERTY()
		Direction LastDirection;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	SnakeStatus status = SnakeStatus::MOVING;
	void DestroySnakeBody();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void BuildSnakeBody(int num = 1);
	void Move();
	void SnakeMustDie();

	UFUNCTION()
		void OnElementOverlap(ASnakeElementBase* element, AActor* actor);


};
