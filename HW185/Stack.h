#pragma once
#include<cstring>

template<typename T>
class Stack
{
public:
	Stack() 
	{
		_array = new T[0];
	};

	~Stack() 
	{
		if (_array != nullptr)
		{
			delete[] _array;
		}
	};

	void Push(T newValue) 
	{
		Resize();
		_array[0] = newValue;
		_size++;
	};

	T Pop() 
	{
		T result = NULL;
		if (_size != 0)
		{
			result = _array[0];
			Resize(false);
		}
		return result;
	};

	T* GetArray()
	{
		return _array != nullptr ? _array : nullptr;
	};

	int GetSize()
	{
		return _size;
	}

private:
	T* _array = nullptr;
	int _size = 0;

	void Resize(bool inc = true) 
	{
		void* src = 0;
		void* dst = 0;
		T* newArray = new T[inc ? _size + 1 : _size - 1];

		if(inc)
		{
			src = _array;
			if (_size == 0)
			{
				dst = newArray;
			}
			else
			{
				dst = newArray + 1;
			}
		}
		else
		{
			src = _array + 1;
			dst = newArray;
			_size--;
		}
		std::memcpy(dst, src, sizeof(T) * _size);
		delete[] _array;
		_array = newArray;
	};

};