// HW185.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include "Stack.h"
#include <iostream>
using namespace std;


int main()
{
    std::cout << "Hello World!\n";
    Stack<int> st;
    st.Push(77);
    st.Push(737);
    st.Push(747);
    st.Push(377);
    st.Push(747);

    int* ar = st.GetArray();
    int sz = st.GetSize();
    for (int i = 0; i != sz; i++)
    {
        cout << ar[i] << "\t";
    }

    cout << endl;

    int y = st.Pop();

    ar = st.GetArray();
    sz = st.GetSize();
    for (int i = 0; i != sz; i++)
    {
        cout << ar[i] << "\t";
    }
}

