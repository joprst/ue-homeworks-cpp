﻿
#include <iostream>
#include <string>
#include "helpers.h"

int main()
{
    std::string strSample = "Simple string";
    std::cout << "String is " << strSample << '\n';
    std::cout << "String lenght is " << strSample.length() << '\n';
    std::cout << "First char " << strSample.at(0) << '\n';
    std::cout << "Last char " << strSample.at(strSample.length() - 1) << '\n';
}

