// HW165.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <time.h>


int main()
{
    const int N = 5;
    int arr[N][N];

    for (int i = 0; i != N; i++)
    {
        for (int j = 0; j != N; j++)
        {
            arr[i][j] = i + j;
            std::cout << i + j;
        }
        std::cout << std::endl;
    }

    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);

    int row = buf.tm_mday % N;
    int summ = 0;

    for (int i = 0; i != N; i++)
    {
        summ += arr[row][i];
    }
    std::cout << summ;
}


