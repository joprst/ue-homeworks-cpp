#include <math.h>
#include "SimpleVector.h"


SimpleVector::SimpleVector(double x, double y, double z) : _x(x), _y(y), _z(z)
{
}

double SimpleVector::GetX()
{
    return _x;
}

double SimpleVector::GetY()
{
    return _y;
}

double SimpleVector::GetZ()
{
    return _z;
}

double SimpleVector::GetVectorModal()
{
    return sqrt(_x * _x + _y * _y + _z * _z);
}
