#pragma once
class SimpleVector
{
private:
    double _x = 0;
    double _y = 0;
    double _z = 0;

public:
    SimpleVector(double x, double y, double z);
    double GetX();
    double GetY();
    double GetZ();
    double GetVectorModal();
};