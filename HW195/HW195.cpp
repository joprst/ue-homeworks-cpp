// HW195.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "Annimal.h"
#include "Cow.h"
#include "Dog.h"
#include "ElGato.h"

int main()
{
    Annimal** zoo = new Annimal*[4];
    zoo[0] = new Dog();
    zoo[1] = new Cow();
    zoo[2] = new ElGato();
    zoo[3] = new Cow();

    for (int i = 0; i != 4; i++)
    {
        zoo[i]->Voice();
        delete zoo[i];
    }

    delete[] zoo;
}


